
import javax.media.opengl.*;
import com.jogamp.opengl.util.*;

import java.util.*;
import Utils.*;
import javafx.geometry.Point3D;
import Creatures.*;

public class Vivarium {
    private int UPDATE_INTERVAL = 1;
    private int FLY_NUMBER = 20;
    private float FOOD_SCALE = 0.2f;
    private float FLY_SCALE = 0.2f;
    private float SPIDER_SCALE = 0.3f;
    
    private Tank tank;
    private Spider Maexxna;
    private Vector<CreatureBase> flies = new Vector<CreatureBase>();
    private Vector<CreatureBase> foods = new Vector<CreatureBase>();
    
    public boolean show_tank = true;    
    public boolean show_Maexxna = true;
    public boolean create_food;
    public int update_timer = 0;

    public Vivarium() {
        Random rand = new Random();
        if(show_tank)
            tank = new Tank(4.0f, 4.0f, 4.0f);
        if(show_Maexxna)
            Maexxna = new Spider(SPIDER_SCALE);
        for(int i = 0 ; i < this.FLY_NUMBER ; i++){
            Point3D randLoc = new Point3D(
                    -1f+ rand.nextFloat() * 2f , 
                    -1f + rand.nextFloat() * 2f , 
                    -1f + rand.nextFloat() * 2f
                    );
            Fly tempFly = new Fly(FLY_SCALE);
            tempFly.position = tempFly.position.add(randLoc);
            boolean collision = false;
            for(CreatureBase creature : flies){
                if(creature.position.distance(tempFly.position) < tempFly.collisionDetector.collisionRadius){
                    collision = true;
                }
            }
            if(!collision){
                flies.add(tempFly);
            }
        }
    }

    public void init(GL2 gl) {
        if(show_tank)
            tank.init(gl);
        if(show_Maexxna)
            Maexxna.init(gl);
        for(CreatureBase creature : flies){
            creature.init(gl);
        }
        create_food = false;
    }

    public void update(GL2 gl) {
        //create food
        if(this.create_food){
            Random rand = new Random();
            Food tempFood = new Food(FOOD_SCALE);
            Point3D randLoc = new Point3D(
                    -1.9f+ rand.nextFloat() * 3.8f , 
                    -1.9f + rand.nextFloat() * 3.8f , 
                    -1.9f + rand.nextFloat() * 3.8f
                    );
            tempFood.position = tempFood.position.add(randLoc);
            boolean collision = false;
            for(CreatureBase creature : flies){
                if(creature.position.distance(tempFood.position) < tempFood.collisionDetector.collisionRadius){
                    collision = true;
                }
            }
            for(CreatureBase food : foods){
                if(food.position.distance(tempFood.position) < tempFood.collisionDetector.collisionRadius){
                    collision = true;
                }
            }
            if(!collision){
                tempFood.init(gl);
                this.foods.add(tempFood);
                this.create_food = false;
            }
        }
        
        //update with time inverval
        update_timer = (update_timer + 1);
        if(update_timer % UPDATE_INTERVAL == 0){
            if(show_tank)
                tank.update(gl);
          //update spider
            if(show_Maexxna){
                Maexxna.update(gl , flies);
            }
            //update flies
            for(CreatureBase creature : flies){
                if(!creature.isEaten){
                    creature.update(gl , flies , foods , Maexxna);
                }
            }
            //update foods
            for(CreatureBase creature : foods){
                if(!creature.isEaten){
                    creature.update(gl , flies , foods , Maexxna);
                }
            }
        }
        
        //remove dead flies
        for(int i = 0 ; i < flies.size() ; i++){
            if(flies.get(i).isEaten){
                flies.remove(i);
            }
        }
        
        //remove dead foods
        for(int i = 0 ; i < foods.size() ; i++){
            if(foods.get(i).isEaten){
                foods.remove(i);
            }
        }
    }

    public void draw(GL2 gl) {
        //draw tank
        if(show_tank)
            tank.draw(gl);
        //draw spider
        if(show_Maexxna)
            Maexxna.draw(gl);
        //draw flies
        for(CreatureBase creature : flies){
                creature.draw(gl);
        }
        //draw foods
        for(CreatureBase food : foods){
                food.draw(gl);
        }
    }
    
    //when key pressed, simply set the bit and add it in update function
    public void addFood(){
        this.create_food = true;
    }
}

package Utils;

import javafx.geometry.Point3D;

public class Quaternion {
    public float s;
    public float[] v;

    public static final float EPSILON = 0.0001f;

    public Quaternion(double _s, double d, double e, double f) {
        v = new float[3];
        set(_s, d, e, f);
    }

    public Quaternion() {
        v = new float[3];
        set(1f, 0f, 0f, 0f);
    }

    public void reset() {
        set(1f, 0f, 0f, 0f);
    }

    public void set(double _s, double d, double e, double f) {
        s = (float) _s;
        v[0] = (float) d;
        v[1] = (float) e;
        v[2] = (float) f;
    }

    /* Multiply two quaternions */
    /* Use Eq. A-79 on p. 806 of Hearn and Baker */
    public Quaternion multiply(Quaternion Q) {
        Quaternion outQ = new Quaternion();

        // s = s1*s2 - v1.v2
        outQ.s = s * Q.s - v[0] * Q.v[0] - v[1] * Q.v[1] - v[2] * Q.v[2];

        // v = s1 v2 + s2 v1 + v1 x v2
        outQ.v[0] = v[1] * Q.v[2] - v[2] * Q.v[1]; // Cross product
        outQ.v[1] = v[2] * Q.v[0] - v[0] * Q.v[2];
        outQ.v[2] = v[0] * Q.v[1] - v[1] * Q.v[0];

        for (int i = 0; i < 3; ++i) // s1 v2 + s2 v1
            outQ.v[i] += s * Q.v[i] + Q.s * v[i];

        return outQ;
    }

    /**
     * apply rotation to an array
     * @param array
     * @return
     */
    public Point3D multiply(final Point3D array){
        float[] matrix = this.to_matrix();
        float[] tempArray = new float[]{(float) array.getX() , (float) array.getY() , (float) array.getZ() , 1f};
        float[] after_rotate = new float[]{0 , 0 , 0 , 0};
        for(int i = 0 ; i < 16 ; i++)
        {
            after_rotate[i / 4] += matrix[i] * tempArray[i % 4];  
        }
//        System.out.printf("Quaternion.java: make sure last digit of array is %f\n", after_rotate[3]);
        Point3D ret = new Point3D(after_rotate[0] , after_rotate[1] , after_rotate[2]);
        return ret;
    }
    /* due to accumulating round-off error, it may be necessary to normalize */
    /* this will ensure that the quaternion is truly unit */
    public void normalize() {
        float mag = magnitude();

        if (mag > EPSILON) {
            s /= mag;
            v[0] /= mag;
            v[1] /= mag;
            v[2] /= mag;
        }
    }

    public float magnitude() {
        return (float) Math.sqrt(s * s + v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    }

    /* Use Eq. 5-107 on p. 273 of Hearn and Baker */
    /* be aware that OpenGL uses column major when specifying the matrix */
    public float[] to_matrix() {
        float a, b, c;
        float[] M = new float[16];

        a = v[0];
        b = v[1];
        c = v[2];

        // Specify the matrix in column major
        M[0] = 1 - 2 * b * b - 2 * c * c; // M[0][0]
        M[1] = 2 * a * b + 2 * s * c; // M[1][0]
        M[2] = 2 * a * c - 2 * s * b; // M[2][0]
        M[3] = 0.0f; // M[3][0]

        M[4] = 2 * a * b - 2 * s * c; // M[0][1]
        M[5] = 1 - 2 * a * a - 2 * c * c; // M[1][1]
        M[6] = 2 * b * c + 2 * s * a; // M[2][1]
        M[7] = 0.0f; // M[3][1]

        M[8] = 2 * a * c + 2 * s * b; // M[0][2]
        M[9] = 2 * b * c - 2 * s * a; // M[1][2]
        M[10] = 1 - 2 * a * a - 2 * b * b; // M[2][2]
        M[11] = 0.0f; // M[3][2]

        M[12] = 0.0f; // M[0][3]
        M[13] = 0.0f; // M[1][3]
        M[14] = 0.0f; // M[2][3]
        M[15] = 1.0f; // M[3][3]

        return M;
    }
    
    public String toString(){
        String ret = new String("Quaternion[");
        ret += Float.toString(s);
        ret += " , ";
        ret += Float.toString(v[0]);
        ret += " , ";
        ret += Float.toString(v[1]);
        ret += " , ";
        ret += Float.toString(v[2]);
        ret += "]";
        return ret;
    }
}

package Utils;

import javafx.geometry.Point3D;

public class CollisionDetector {
    public float collisionRadius;
    public float wallCollisionRadius;
    public Point3D min;
    public Point3D max;
    public Point3D position;
    public enum wallCollisionType{xMin , xMax , yMin , yMax , zMin , zMax , noCollisionX, noCollisionY, noCollisionZ}
    
    public CollisionDetector(){
        collisionRadius = 0;
        wallCollisionRadius = 0;
        min = new Point3D(0 , 0 , 0);
        max = new Point3D(0 , 0 , 0);
        position = new Point3D(0 , 0 , 0);
    }
    
    public void setMin(Point3D minPoint){
        this.min = minPoint;
    }
    
    public void setMax(Point3D maxPoint){
        this.max = maxPoint;
    }
    
    public void setPosition(Point3D setPosition){
        this.position = setPosition;
    }
    
    public void setCollisionRadius(float radius){
        this.collisionRadius = radius;
    }
    
    public wallCollisionType wallCollisionTestX(){
        if(this.position.getX() - this.wallCollisionRadius <= -2.0f)
            return wallCollisionType.xMin;
        if(this.position.getX() + this.wallCollisionRadius >= 2.0f)
            return wallCollisionType.xMax;
        return wallCollisionType.noCollisionX;
    }
    
    public wallCollisionType wallCollisionTestY(){
        if(this.position.getY() - this.wallCollisionRadius <= -2.0f)
            return wallCollisionType.yMin;
        if(this.position.getY() + this.wallCollisionRadius >= 2.0f)
            return wallCollisionType.yMax;
        return wallCollisionType.noCollisionY;
    }
    
    public wallCollisionType wallCollisionTestZ(){
        if(this.position.getZ() - this.wallCollisionRadius <= -2.0f)
            return wallCollisionType.zMin;
        if(this.position.getZ() + this.wallCollisionRadius >= 2.0f)
            return wallCollisionType.zMax;
        return wallCollisionType.noCollisionZ;
    }
    
    public boolean outSideTest(){
        if(this.position.getX()  >= 2.0f || this.position.getX() <= -2.0f
            || this.position.getY()  >= 2.0f || this.position.getY() <= -2.0f
            || this.position.getZ()  >= 2.0f || this.position.getZ() <= -2.0f
                ){
            return true;
        }
        return false;
    }
}

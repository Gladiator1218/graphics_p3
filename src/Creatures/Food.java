package Creatures;

import javax.media.opengl.*;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.util.gl2.GLUT;

import Utils.CollisionDetector;

import java.util.*;

public class Food extends CreatureBase{
    private float scale;
    private int food_object;
    private float FOOD_RADIUS = 0.1f;


    public Food(float scale_) {
        this.isEaten = false;
        this.collisionDetector = new CollisionDetector();
        scale = scale_;
        this.collisionDetector.setCollisionRadius(FOOD_RADIUS * scale);
    }

    public void init(GL2 gl) {
        GLUT glut = new GLUT();
        food_object = gl.glGenLists(1);
        gl.glNewList(food_object, GL2.GL_COMPILE);
            gl.glScalef(scale, scale, scale);
            glut.glutSolidSphere(FOOD_RADIUS, 18, 36);
        gl.glEndList();
    }

    @Override
    public void update(GL2 gl , Vector<CreatureBase> flies , Vector<CreatureBase> foods , Spider spider) {
        for(CreatureBase creature : flies){
            if(creature.position.distance(this.position) < this.collisionDetector.collisionRadius + creature.collisionDetector.collisionRadius){
                this.isEaten = true;
            }
        }
        if(spider.position.distance(this.position) < this.collisionDetector.collisionRadius + spider.collisionDetector.collisionRadius){
            this.isEaten = true;
        }
    }

    public void draw(GL2 gl) {
        if(this.isEaten){
            return;
        }
        gl.glPushMatrix();
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
            gl.glTranslated(this.position.getX(), this.position.getY(), this.position.getZ());
            gl.glColor3f(0.85f, 0.55f, 0.20f);
            gl.glCallList(food_object);
        gl.glPopAttrib();
        gl.glPopMatrix();
    }
}

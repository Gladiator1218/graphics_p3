package Creatures;

import javax.media.opengl.*;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.util.gl2.GLUT;

import Utils.CollisionDetector;
import Utils.CollisionDetector.wallCollisionType;
import Utils.Quaternion;
import javafx.geometry.Point3D;

import java.util.*;

public class Spider extends CreatureBase{
    private float SPIDER_SPEED = 0.02f;
    private float BODY_RADIUS = 0.4f;
    private float HEAD_RADIUS = 0.2f;
    private float EYE_RADIUS = 0.02f;

    private float DIST_RADIUS = 0.01f;
    private float MID_RADIUS = 0.02f;
    private float IN_RADIUS = 0.03f;

    private float DIST_LENGTH = 0.3f;
    private float MID_LENGTH = 0.2f;
    private float IN_LENGTH = 0.2f;
    private Point3D INIT_POSITION = new Point3D(0f, 0f, 0f);
    private int TIME_INTERVAL = 5;
    
    private int spider_torso;
    private int spider_legs_still;
    private int spider_legs_move;
    private float scale;
    private int movingFrame;
    private int movingFrameSize = 4;
    
    public Spider(float scale_) {
        this.scale = scale_;
        this.collisionDetector = new CollisionDetector();
        this.collisionDetector.collisionRadius = this.scale * (2 * HEAD_RADIUS);
        this.collisionDetector.wallCollisionRadius = this.scale * 0.8f * (DIST_LENGTH + MID_LENGTH + IN_LENGTH);
        this.movingFrame = 0;
        this.isMoving = true;
        this.moveSpeed = 0.1f;
        this.isEaten = false;
        this.type = creatureType.Spider;
        this.position = this.position.add(0f, 0 , 0);//-2 + this.BODY_RADIUS * 0.7f * scale, 0f);
        this.lastPosition = this.position;
        this.lastLookAtVector = this.lookAtVector;
    }

    public void init(GL2 gl) {
        GLUT glut = new GLUT();
        spider_torso = gl.glGenLists(1);
        spider_legs_still = gl.glGenLists(1);
        spider_legs_move = gl.glGenLists(4);
        // torso list
        /*
         * Q:do i need a pushMatrix here? A:guess this is just creating list
         * instead of actually draw things so no need for pushMatrix. Instead,
         * use pushMatrix before you call list.
         */
        gl.glNewList(spider_torso, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        // black body
        gl.glColor3f(0.2f, 0.20f, 0.20f);
        gl.glScalef(scale, scale, scale);
        // body
        gl.glPushMatrix();
        gl.glTranslatef(0f, 0f, -BODY_RADIUS);//p' = MTRp
        gl.glScalef(0.8f, 0.7f, 1.0f);
        glut.glutSolidSphere(BODY_RADIUS, 36, 18);
        gl.glPopMatrix();
        // head
        gl.glPushMatrix();
        gl.glTranslatef(0f, 0f, HEAD_RADIUS / 2);
        gl.glScalef(0.8f, 0.8f, 1.0f);
        glut.glutSolidSphere(HEAD_RADIUS, 36, 18);
        gl.glPopMatrix();
        // eyes
        gl.glColor3f(1.0f, 0.0f, 0.0f);
        gl.glPushMatrix();
        gl.glTranslatef(3f * this.EYE_RADIUS, this.EYE_RADIUS, 0.3f);
        glut.glutSolidSphere(EYE_RADIUS, 36, 18);
        gl.glTranslatef(-2f * this.EYE_RADIUS, this.EYE_RADIUS, -0.05f * this.EYE_RADIUS);
        glut.glutSolidSphere(EYE_RADIUS, 36, 18);
        gl.glTranslatef(-2f * this.EYE_RADIUS, 0.0f, 0f);
        glut.glutSolidSphere(EYE_RADIUS, 36, 18);
        gl.glTranslatef(-2f * this.EYE_RADIUS, -1f * this.EYE_RADIUS, 0.05f * this.EYE_RADIUS);
        glut.glutSolidSphere(EYE_RADIUS, 36, 18);
        gl.glPopMatrix();

        gl.glPopAttrib();
        gl.glEndList();

        // leg list1
        gl.glNewList(spider_legs_still, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glColor3f(0.2f, 0.20f, 0.20f);
        gl.glPushMatrix();
        //leg1 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(135, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg2 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(115, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg3 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(75, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg4 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(35, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg5 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(-135, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg6 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(-115, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg7 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(-75, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg8 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(-35, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        gl.glPopMatrix();
        gl.glPopAttrib();
        gl.glEndList();
        // leg move frame1
        gl.glNewList(spider_legs_move + 0, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glColor3f(0.2f, 0.20f, 0.20f);
        gl.glPushMatrix();
        //leg1 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(145, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg2 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(105, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg3 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(85, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg4 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(25, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg5 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(-145, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg6 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(-105, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg7 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(-85, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg8 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(-25, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        gl.glPopMatrix();
        gl.glPopAttrib();
        gl.glEndList();
        // leg move frame2
        gl.glNewList(spider_legs_move + 1, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glColor3f(0.2f, 0.20f, 0.20f);
        gl.glPushMatrix();
        //leg1 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(125, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg2 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(123, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg3 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(65, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg4 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(45, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg5 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(-125, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg6 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(-123, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg7 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(-65, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg8 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(-45, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        gl.glPopMatrix();
        gl.glPopAttrib();
        gl.glEndList();
        // leg move frame3
        gl.glNewList(spider_legs_move + 2, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glColor3f(0.2f, 0.20f, 0.20f);
        gl.glPushMatrix();
        //leg1 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(125, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg2 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(123, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg3 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(65, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg4 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(45, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg5 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(-125, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg6 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(-123, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg7 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(-65, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg8 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(-45, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        gl.glPopMatrix();
        gl.glPopAttrib();
        gl.glEndList();
        // leg move frame4
        gl.glNewList(spider_legs_move + 3, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glColor3f(0.2f, 0.20f, 0.20f);
        gl.glPushMatrix();
        //leg1 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(145, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg2 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(105, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg3 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(85, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg4 in
            gl.glPushMatrix();
            gl.glTranslatef(0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(25, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg5 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 0f);
            gl.glRotatef(-145, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg6 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 2f * this.IN_RADIUS);
            gl.glRotatef(-105, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg7 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 4f * this.IN_RADIUS);
            gl.glRotatef(-85, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        //leg8 in
            gl.glPushMatrix();
            gl.glTranslatef(-0.65f * this.HEAD_RADIUS, 0f, 6f * this.IN_RADIUS);
            gl.glRotatef(-25, 0f, 1f, 0f);
            gl.glRotatef(-25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.IN_RADIUS, this.IN_LENGTH, 36, 28);
            //mid
            gl.glTranslatef(0f, 0f, this.IN_LENGTH);
            gl.glRotatef(25, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.MID_RADIUS, this.MID_LENGTH, 36, 28);
            //dist
            gl.glTranslatef(0f, 0f, this.MID_LENGTH);
            gl.glRotatef(15f, 1f, 0f, 0f);
            gl.glRotatef(0, 0f, 1f, 0f);
            gl.glRotatef(0, 0f, 0f, 1f);
            glut.glutSolidCylinder(this.DIST_RADIUS, this.DIST_LENGTH, 36, 28);
            gl.glPopMatrix();
        gl.glPopMatrix();
        gl.glPopAttrib();
        gl.glEndList();
    }
    
    @Override
    public void update(GL2 gl , Vector<CreatureBase> flies) {
//        if(this.collisionDetector.outSideTest()){
//            System.out.printf("spider outsideTest.true\n");
//            this.resetPosition();
//        }
        //chase the closest target in creatures
//        System.out.printf("%.3f , %.3f , %.3f\n" , this.position.getX() , this.position.getY() , this.position.getZ());
        float distance = Float.MAX_VALUE;
        int tempI = 0;
        for(int i = 0 ; i < flies.size() ; i++){
            if(!flies.get(i).isEaten && flies.get(i).position.distance(this.position) < distance){
                tempI = i;
                distance = (float) flies.get(i).position.distance(this.position);
                if(distance < this.collisionDetector.collisionRadius + flies.get(i).collisionDetector.collisionRadius){
                    flies.get(i).isEaten = true;
                    distance = Float.MAX_VALUE;
                }
            }
        }
        
        if(flies.size() > 0){
            this.rotateMatrix = this.rotateToDestCoord(flies.get(tempI).position);
        }
        
        if(flies.size() == 0){
            this.isMoving = false;
        }
        if(isMoving)
        {
            //System.out.println("===========Spider Wall Collision=================");
            this.wallCollisionHandler();
            //System.out.println("===========Spider Wall Collision End=============");
            movingFrame += 1;
            movingFrame = movingFrame % (movingFrameSize * TIME_INTERVAL);
            this.lastPosition = this.position;
            this.lookAtVector = this.lookAtVector.normalize();
            if(
                this.position.getX() + this.lookAtVector.getX() * SPIDER_SPEED < 2.0f && this.position.getX() + this.lookAtVector.getX() * SPIDER_SPEED > -2.0f &&
                this.position.getY() + this.lookAtVector.getY() * SPIDER_SPEED < 2.0f && this.position.getY() + this.lookAtVector.getY() * SPIDER_SPEED > -2.0f &&
                this.position.getZ() + this.lookAtVector.getZ() * SPIDER_SPEED < 2.0f && this.position.getZ() + this.lookAtVector.getZ() * SPIDER_SPEED > -2.0f
                    ){
                this.position = this.position.add(this.lookAtVector.getX() * SPIDER_SPEED , this.lookAtVector.getY() * SPIDER_SPEED , this.lookAtVector.getZ() * SPIDER_SPEED);
            }
            else{
                this.lookAtVector = new Point3D(-this.lookAtVector.getX() , -this.lookAtVector.getY() , -this.lookAtVector.getZ());
                this.rotateToDestCoord(this.position.add(lookAtVector));
                this.position = this.position.add(this.lookAtVector.getX() * SPIDER_SPEED , this.lookAtVector.getY() * SPIDER_SPEED , this.lookAtVector.getZ() * SPIDER_SPEED);
            }
        }
    }

    @Override
    public void draw(GL2 gl) {
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glPushMatrix(); 
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glTranslated(this.position.getX(), this.position.getY(), this.position.getZ());
        gl.glMultMatrixf(this.rotateMatrix , 0);
        
        
        gl.glCallList(spider_torso);
        //init position
        if(isMoving)
        {
            gl.glCallList(spider_legs_move + movingFrame / TIME_INTERVAL);
        }
        else
        {
            gl.glCallList(spider_legs_still);
        }
        gl.glPopAttrib();
        gl.glPopMatrix();
    }
    
    @Override
    public void wallCollisionHandler(){
        this.lastLookAtVector = this.lookAtVector;
        this.collisionDetector.setPosition(this.position);
        wallCollisionType tempX = this.collisionDetector.wallCollisionTestX();
        wallCollisionType tempY = this.collisionDetector.wallCollisionTestY();
        wallCollisionType tempZ = this.collisionDetector.wallCollisionTestZ();
        if(tempX == wallCollisionType.xMin || tempX == wallCollisionType.xMax){
            //System.out.println("reflect X");
            this.lookAtVector = new Point3D(-this.lookAtVector.getX() , this.lookAtVector.getY() , this.lookAtVector.getZ());
        }
        if(tempY == wallCollisionType.yMin || tempY == wallCollisionType.yMax){
            //System.out.println("reflect Y");
            this.lookAtVector = new Point3D(this.lookAtVector.getX() , -this.lookAtVector.getY() , this.lookAtVector.getZ());
        }
        if(tempZ == wallCollisionType.zMin || tempZ == wallCollisionType.zMax){
            //System.out.println("reflect Z");
            this.lookAtVector = new Point3D(this.lookAtVector.getX() , this.lookAtVector.getY() , -this.lookAtVector.getZ());
        }
    }
}

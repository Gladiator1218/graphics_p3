package Creatures;

import javax.media.opengl.*;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.util.gl2.GLUT;

import java.util.*;

public class Teapot {
    private int teapot_object;
    private float scale;
    private float angle;
    private int frame;

    public Teapot(float scale_) {
        scale = scale_;
        angle = 0;
        frame = 0;
    }

    public void init(GL2 gl) {

        teapot_object = gl.glGenLists(2);
        gl.glNewList(teapot_object, GL2.GL_COMPILE);

        // due to a bug in glutSolidTeapot, triangle vertices are in CW order
        gl.glFrontFace(GL.GL_CW);

        gl.glRotatef(0f, 0f, 1f, 0f);

        // create the teapot triangles
        GLUT glut = new GLUT();
        glut.glutSolidTeapot(scale);

        // return to regular CCW order
        gl.glFrontFace(GL.GL_CCW);
        gl.glEndList();

        gl.glNewList(teapot_object + 1, GL2.GL_COMPILE);

        // due to a bug in glutSolidTeapot, triangle vertices are in CW order
        gl.glFrontFace(GL.GL_CW);

        gl.glRotatef(50f, 1f, 1f, 0f);

        // create the teapot triangles
        glut.glutSolidTeapot(scale);

        // return to regular CCW order
        gl.glFrontFace(GL.GL_CCW);
        gl.glEndList();

    }

    public void update(GL gl) {
        angle += 5;
        frame += 1;
        frame = frame % 2;
    }

    public void draw(GL2 gl) {
        gl.glPushMatrix();
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        // gl.glRotatef( angle, 1.0f, 1.0f, 1.0f );
        gl.glColor3f(0.85f, 0.55f, 0.20f); // Orange
        // System.out.printf("%d\n",frame);
        gl.glCallList(teapot_object);
        gl.glCallList(teapot_object + 1);
        gl.glPopAttrib();
        gl.glPopMatrix();
    }
}

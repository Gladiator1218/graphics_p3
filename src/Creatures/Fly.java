package Creatures;

import javax.media.opengl.*;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.util.gl2.GLUT;

import Creatures.CreatureBase.creatureType;
import Utils.CollisionDetector;
import javafx.geometry.Point3D;

import java.util.*;

public class Fly extends CreatureBase{
    private float FLY_SPEED = 0.019f;
    private float BODY_RADIUS = 0.4f;
    private float HEAD_RADIUS = 0.2f;
    private float WING_RADIUS = 0.4f;
    private int TIME_INTERVAL = 2;
    
    private int fly_torso;
    private int fly_wings_still;
    private int fly_wings_move;
    private float scale;
    private int movingFrame;
    private int movingFrameSize = 2;
    
    public Fly(float scale_) {
        this.isMoving = true;
        this.scale = scale_;
        this.collisionDetector = new CollisionDetector();
        this.collisionDetector.collisionRadius = scale * (2 * BODY_RADIUS);
        this.collisionDetector.wallCollisionRadius = 0.3f * 0.7f * 0.7f;
        this.movingFrame = 0;
        this.moveSpeed = 0.09f;
        this.isEaten = false;
        this.type = creatureType.Fly;
        this.lastPosition = this.position;
        this.lastLookAtVector = this.lookAtVector;
    }

    public void init(GL2 gl) {
        GLUT glut = new GLUT();
        fly_torso = gl.glGenLists(1);
        fly_wings_still = gl.glGenLists(1);
        fly_wings_move = gl.glGenLists(2);
        // torso list
        gl.glNewList(fly_torso, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);

        // black body
        gl.glColor3f(0.4f, 0.4f, 0f);
        gl.glScalef(scale, scale, scale);
        // body
        gl.glPushMatrix();
        gl.glTranslatef(0f, 0f, -0.4f);
        gl.glScalef(0.4f, 0.4f, 1.0f);
        glut.glutSolidSphere(BODY_RADIUS, 36, 18);
        gl.glPopMatrix();
        // head
        gl.glPushMatrix();
        gl.glTranslatef(0f, 0f, HEAD_RADIUS - 0.1f);
        gl.glScalef(.6f, .6f, .6f);
        glut.glutSolidSphere(HEAD_RADIUS, 36, 18);
        gl.glPopMatrix();

        gl.glPopAttrib();
        gl.glEndList();

        // wings still list
        gl.glNewList(fly_wings_still, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glColor3f(0.4f, 0.4f, 0f);
        gl.glPushMatrix();
        //leg1 in
            gl.glPushMatrix();
            gl.glTranslatef(-WING_RADIUS * 0.43f , WING_RADIUS * 0.43f, -WING_RADIUS);
            gl.glRotatef(15, 0f, 1f, 0f);
            gl.glRotatef(15, 1f, 0f, 0f);
            gl.glRotatef(45, 0f, 0f, 1f);
            gl.glScalef(0.4f, .05f, 1f);
            glut.glutSolidSphere(WING_RADIUS, 36, 18);
            gl.glPopMatrix();
        //leg2 in
            gl.glPushMatrix();
            gl.glTranslatef(WING_RADIUS * 0.43f , WING_RADIUS * 0.43f, -WING_RADIUS);
            gl.glRotatef(-15, 0f, 1f, 0f);
            gl.glRotatef(15, 1f, 0f, 0f);
            gl.glRotatef(-45, 0f, 0f, 1f);
            gl.glScalef(0.4f, .05f, 1f);
            glut.glutSolidSphere(WING_RADIUS, 36, 18);
            gl.glPopMatrix();
        gl.glPopMatrix();
        gl.glPopAttrib();
        gl.glEndList();
        // wings move frame1
        gl.glNewList(fly_wings_move + 0, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glColor3f(0.2f, 0.20f, 0.20f);
        gl.glPushMatrix();
        //leg1 in
            gl.glPushMatrix();
            gl.glTranslatef(-WING_RADIUS * 0.5f , WING_RADIUS * 0.5f, -WING_RADIUS * 0.5f);
            gl.glRotatef(75, 0f, 1f, 0f);
            gl.glRotatef(45, 1f, 0f, 0f);
            gl.glRotatef(0f , 0f, 0f, 1f);
            gl.glScalef(0.4f, .05f, 1f);
            glut.glutSolidSphere(WING_RADIUS, 36, 18);
            gl.glPopMatrix();
        //leg2 in
            gl.glPushMatrix();
            gl.glTranslatef(WING_RADIUS * 0.5f , WING_RADIUS * 0.5f, -WING_RADIUS * 0.5f);
            gl.glRotatef(-75 , 0f, 1f, 0f);
            gl.glRotatef(45 , 1f, 0f, 0f);
            gl.glRotatef(0f , 0f, 0f, 1f);
            gl.glScalef(0.4f, .05f, 1f);
            glut.glutSolidSphere(WING_RADIUS, 36, 18);
            gl.glPopMatrix();
        gl.glPopMatrix();
        gl.glPopAttrib();
        gl.glEndList();
        // wings move frame2
        gl.glNewList(fly_wings_move + 1, GL2.GL_COMPILE);
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glColor3f(0.2f, 0.20f, 0.20f);
        gl.glPushMatrix();
            //leg1 in
            gl.glPushMatrix();
            gl.glTranslatef(-WING_RADIUS * 0.7f , -WING_RADIUS * 0.2f, -WING_RADIUS * 0.5f);
            gl.glRotatef(75, 0f, 1f, 0f);
            gl.glRotatef(-15, 1f, 0f, 0f);
            gl.glRotatef(0f , 0f, 0f, 1f);
            gl.glScalef(0.4f, .05f, 1f);
            glut.glutSolidSphere(WING_RADIUS, 36, 18);
            gl.glPopMatrix();
        //leg2 in
            gl.glPushMatrix();
            gl.glTranslatef(WING_RADIUS * 0.7f , -WING_RADIUS * 0.2f, -WING_RADIUS * 0.5f);
            gl.glRotatef(-75 , 0f, 1f, 0f);
            gl.glRotatef(-15 , 1f, 0f, 0f);
            gl.glRotatef(0f , 0f, 0f, 1f);
            gl.glScalef(0.4f, .05f, 1f);
            glut.glutSolidSphere(WING_RADIUS, 36, 18);
            gl.glPopMatrix();
        gl.glPopMatrix();
        gl.glPopAttrib();
        gl.glEndList();
    }
    
    @Override
    public void update(GL2 gl , Vector<CreatureBase> flies  , Vector<CreatureBase> foods, Spider spider) 
    {
        //check if it's outside the tank
        this.collisionDetector.position = this.position;
//        if(this.collisionDetector.outSideTest()){
//            System.out.printf("resetting fly\n");
//            this.resetPosition();
//        }
        
        //check spider coming
        if(this.position.distance(spider.position) <= this.ALERT_DISTANCE_FLY && !isMoving){
            this.startEscape(spider);
        }
        


        if(isMoving)
        {
            //food affect
            this.foodTrackRevision(foods);
            //spider affect
            this.spiderTrackRevision(spider);
            //flock affect
            this.flockTrackRevision(flies);
            //add noise
            if(this.ENABLE_NOISE){
                Random rand = new Random();
                this.noise = new Point3D(
                        -this.NOISE_SCALE + this.NOISE_SCALE * rand.nextFloat(),
                        -this.NOISE_SCALE + this.NOISE_SCALE * rand.nextFloat(),
                        -this.NOISE_SCALE + this.NOISE_SCALE * rand.nextFloat()
                    );
                this.rotateToDestCoord(this.position.add(this.noise));
            }
            //check wallCollision
            this.wallCollisionHandler();
            //normalize
            this.lookAtVector = this.lookAtVector.normalize();
            this.rotateMatrix = this.rotateToDestCoord(this.position.add(this.lookAtVector));
            //check collision between flies
//          boolean collision = false;
//            for(CreatureBase fly : flies){
//                if(fly == this){
//                    this.lastLookAtVector = this.lookAtVector;
//                    this.isMoving = true;
//                    continue;
//                }
//                if(this.position.distance(fly.position) <= this.collisionDetector.collisionRadius * 2){
//                    collision = true;
//                }
//                else{
//                    this.lastLookAtVector = this.lookAtVector;
//                    this.isMoving = true;
//                }
//            }
            if(
                    this.position.getX() + this.collisionDetector.wallCollisionRadius + this.lookAtVector.getX() * FLY_SPEED < 2.0f && this.position.getX() - this.collisionDetector.wallCollisionRadius + this.lookAtVector.getX() * FLY_SPEED > -2.0f &&
                    this.position.getY() + this.collisionDetector.wallCollisionRadius + this.lookAtVector.getY() * FLY_SPEED < 2.0f && this.position.getY() - this.collisionDetector.wallCollisionRadius + this.lookAtVector.getY() * FLY_SPEED > -2.0f &&
                    this.position.getZ() + this.collisionDetector.wallCollisionRadius + this.lookAtVector.getZ() * FLY_SPEED < 2.0f && this.position.getZ() - this.collisionDetector.wallCollisionRadius + this.lookAtVector.getZ() * FLY_SPEED > -2.0f
                        ){
                    this.position = this.position.add(this.lookAtVector.getX() * FLY_SPEED , this.lookAtVector.getY() * FLY_SPEED , this.lookAtVector.getZ() * FLY_SPEED);
                }
            else{
                this.lookAtVector = new Point3D(-this.lookAtVector.getX() , -this.lookAtVector.getY() , -this.lookAtVector.getZ());
                this.rotateToDestCoord(this.position.add(lookAtVector));
                this.position = this.position.add(this.lookAtVector.getX() * FLY_SPEED , this.lookAtVector.getY() * FLY_SPEED , this.lookAtVector.getZ() * FLY_SPEED);
            }
        }
        movingFrame += 1;
        movingFrame = movingFrame % (movingFrameSize * TIME_INTERVAL);
    }

    @Override
    public void draw(GL2 gl) {
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glPushMatrix();
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glTranslated(this.position.getX(), this.position.getY(), this.position.getZ());
        gl.glMultMatrixf(this.rotateMatrix , 0);
        gl.glCallList(fly_torso);
        if(isMoving)
        {
            gl.glCallList(fly_wings_move + movingFrame / TIME_INTERVAL);
        }
        else
        {
            gl.glCallList(fly_wings_still);
        }
        gl.glPopAttrib();
        gl.glPopMatrix();
    }
    
    public void startEscape(Spider spider){
        this.lastLookAtVector = this.lookAtVector;
        this.lookAtVector = this.position.subtract(spider.position);
        this.rotateMatrix = this.rotateToDestCoord(this.position.add(this.lookAtVector));
        this.lookAtVector = this.lookAtVector.normalize();
        this.isMoving = true;
    }
}

package Creatures;

import javafx.geometry.Point3D;

import java.util.Vector;

import javax.media.opengl.GL2;

import Utils.*;
import Utils.CollisionDetector.wallCollisionType;

public class CreatureBase {
    protected float ALERT_DISTANCE_FLY = 1.2f;
    protected boolean ENABLE_NOISE = false;
    protected float NOISE_SCALE = 0.001f;
    private int FOOD_WEIGHT = 1;
    private int SPIDER_WEIGHT = 50;
    private float FLOCK_DIST = 0.5f;
    private float WALL_DIST = 1f;
    
    //moving digit
    public boolean isMoving;
    //current position
    public Point3D lastPosition = new Point3D(0 , 0 , 0);
    public Point3D position = new Point3D(0 , 0 , 0);
    //eye look at
    protected float[] rotateMatrix;
    
    public Point3D lastLookAtVector = new Point3D(0 , 0 , 1);
    public Point3D lookAtVector = new Point3D(0 , 0 , 1);
    public Point3D upVector = new Point3D(0 , 1 , 0);
    public Point3D leftVector = new Point3D(1 , 0 , 0);
    public Point3D noise = new Point3D(0 , 0 , 0);
//    public Quaternion lookAtQuaternion = new Quaternion(1 , 0 , 0 , 0);
    //move speed
    public float moveSpeed;
    //eaten
    public boolean isEaten;
    //creature type enum
    public enum creatureType{Fly , Spider};
    //creature type
    public creatureType type;
    //collision class
    public CollisionDetector collisionDetector = new CollisionDetector();
    
    public void init(GL2 gl) {}
    
    public void draw(GL2 gl) {}
    
    //update for spider
    public void update(GL2 gl, Vector<CreatureBase> flies) {}
    
    //update for fly
    public void update(GL2 gl , Vector<CreatureBase> flies  , Vector<CreatureBase> foods, Spider spider) {}

    public float[] rotateToDestCoord(Point3D destCoord)
    {
        Point3D destVector = destCoord.subtract(this.position);
        //correspond to z axis
        this.lookAtVector = destVector.normalize();
        Point3D yAxis = new Point3D(0 , 1 , 0);
        //correspond to x axis
        this.leftVector = yAxis.crossProduct(this.lookAtVector).normalize();
        //correspond to y axis
        this.upVector = this.lookAtVector.crossProduct(leftVector).normalize();
        
        float[] M = new float[16];
        M[0] = (float) leftVector.getX(); // M[0][0]
        M[1] = (float) leftVector.getY(); // M[1][0]
        M[2] = (float) leftVector.getZ(); // M[2][0]
        M[3] = 0.0f; // M[3][0]

        M[4] = (float) upVector.getX(); // M[0][1]
        M[5] = (float) upVector.getY(); // M[1][1]
        M[6] = (float) upVector.getZ(); // M[2][1]
        M[7] = 0.0f; // M[3][1]

        M[8] = (float) lookAtVector.getX(); // M[0][2]
        M[9] = (float) lookAtVector.getY(); // M[1][2]
        M[10] = (float) lookAtVector.getZ(); // M[2][2]
        M[11] = 0.0f; // M[3][2]

        M[12] = 0.0f; // M[0][3]
        M[13] = 0.0f; // M[1][3]
        M[14] = 0.0f; // M[2][3]
        M[15] = 1.0f; // M[3][3]
        return M;
    }
    
    //add food affect into lookAtVector
    public void foodTrackRevision(Vector<CreatureBase> foods){
    	//find closest food in alert radius, if no food in radius, return
        Food tempFood = null;
        float tempDist = Float.MAX_VALUE;
        for(CreatureBase food : foods){
            if(food.position.distance(this.position) < tempDist && food.position.distance(this.position) < this.ALERT_DISTANCE_FLY){
                tempFood = (Food) food;
                tempDist = (float) food.position.distance(this.position);
            }
        }
        if(tempFood == null){
            return;
        }
    	//add food vector * its weight to current lookAtVector
        Point3D tempVector = tempFood.position.subtract(this.position);
        this.lookAtVector = this.lookAtVector.add(tempVector.multiply(FOOD_WEIGHT));
    }
    
    //add predetor affect into lookAtVector
    public void spiderTrackRevision(Spider spider){
        if(this.position.distance(spider.position) <= this.ALERT_DISTANCE_FLY){
            Point3D tempVector = this.position.subtract(spider.position);
            this.lookAtVector = this.lookAtVector.add(tempVector).multiply(SPIDER_WEIGHT);
        }
    }
    
    public void flockTrackRevision(Vector<CreatureBase> flies){
        this.alignmentTrackRevision(flies);
        this.separationTrackRevision(flies);
        this.cohesionTrackRevision(flies);
    }
    
    //get align with flies in flock distance
    public void alignmentTrackRevision(Vector<CreatureBase> flies){
        int tempAmount = 0;
        float tempDist = 0;
        Point3D tempVector = new Point3D(0 , 0 , 0);
        //check every object except self
        for(CreatureBase fly : flies){
            if(fly == this){
                continue;
            }
            tempDist = (float) this.position.distance(fly.position);
            if(tempDist <= this.FLOCK_DIST)
            {
                tempAmount = tempAmount + 1;
                tempVector = tempVector.add(fly.lookAtVector.normalize());
            }
        }
        //U R the only survivor
        if(tempAmount == 0){
            return;
        }
        if(tempVector.normalize() != this.lastLookAtVector.multiply(-1)){
            this.lookAtVector = this.lookAtVector.add(tempVector.normalize());
        }
        else{
            this.lookAtVector = tempVector.normalize();
        }
    }

    //IDK WTF IS SEPARATION
    public void separationTrackRevision(Vector<CreatureBase> flies){
        for(CreatureBase fly : flies){
            if(fly == this){
                continue;
            }
            float tempDist = (float) this.position.distance(fly.position);
            if(tempDist <= this.collisionDetector.collisionRadius * 2)
            {
                float weight = 1 - tempDist / this.FLOCK_DIST;
                this.lookAtVector = this.lookAtVector.add(this.position.subtract(fly.position).normalize().multiply(weight * 2));
            }
        }
    }

    //get together
    public void cohesionTrackRevision(Vector<CreatureBase> flies){
        float tempAmount = 0;
        float tempDist = 0;
        Point3D tempPosition = new Point3D(0 , 0 , 0);
      //check every object except self
        for(CreatureBase fly : flies){
            if(fly == this){
                continue;
            }
            tempDist = (float) this.position.distance(fly.position);
            if(tempDist <= this.FLOCK_DIST)
            {
                tempAmount = tempAmount + 1;
                tempPosition = tempPosition.add(fly.position);
            }
        }
        //U R the only survivor
        if(tempAmount == 0){
            return;
        }
        float weight = tempDist / this.FLOCK_DIST;
        tempPosition = tempPosition.multiply(1.0f / tempAmount);
        this.lookAtVector = this.lookAtVector.add(tempPosition.subtract(this.position).normalize().multiply(weight));
    }
    
    public void wallCollisionHandler(){
        this.lastLookAtVector = this.lookAtVector;
        this.collisionDetector.setPosition(this.position);
        Point3D tempVector = new Point3D(0 , 0 , 0);
        
        if(this.position.getX() - 0.2 + 2.0 < this.WALL_DIST){
            this.lookAtVector = this.lookAtVector.add((1 / (this.position.getX() + 1.8)) , 0 , 0);
        }
        if(2.0 - 0.2 - this.position.getX() < this.WALL_DIST){
            this.lookAtVector = this.lookAtVector.add((-1 / (1.8 - this.position.getX())) , 0 , 0);
        }
        
        if(this.position.getY() - 0.2 + 2.0 < this.WALL_DIST){
            this.lookAtVector = this.lookAtVector.add(0 , (1 / (this.position.getY() + 1.8)) , 0);
        }
        if(2.0 - 0.2 - this.position.getY() < this.WALL_DIST){
            this.lookAtVector = this.lookAtVector.add(0 , (-1 / (1.8 - this.position.getY())) , 0);
        }
        
        if(this.position.getZ() - 0.2 + 2.0 < this.WALL_DIST){
            this.lookAtVector = this.lookAtVector.add(0 , 0 , (1 / (this.position.getZ() + 1.8)));
        }
        if(2.0 - 0.2 - this.position.getZ() < this.WALL_DIST){
            this.lookAtVector = this.lookAtVector.add(0 , 0 , (-1 / (1.8 - this.position.getZ())));
        }
        if(tempVector.equals(Point3D.ZERO)){
            return;
        }
        else{
            this.lookAtVector = this.lookAtVector.add(tempVector);
        }
    }
    
    public void resetPosition(){
        if(this.position.getX() > 2.0f){
            this.position = this.position.subtract(4 , 0 , 0);
        }
        if(this.position.getX() < -2.0f){
            this.position = this.position.add(4 , 0 , 0);
        }
        if(this.position.getY() > 2.0f){
            this.position = this.position.subtract(0 , 4 , 0);
        }
        if(this.position.getY() < -2.0f){
            this.position = this.position.add(0 , 4 , 0);
        }
        if(this.position.getZ() > 2.0f){
            this.position = this.position.subtract(0 , 0 , 4);
        }
        if(this.position.getZ() < -2.0f){
            this.position = this.position.add(0 , 0 , 4);
        }
        
        this.lastLookAtVector = this.lookAtVector;
        this.collisionDetector.position = this.position;
    }
}
